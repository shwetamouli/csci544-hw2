__author__ = 'shwetamouli'
import sys
import argparse
import os

parser = argparse.ArgumentParser(description='Adding heldout dev file')
parser.add_argument('-hd', action="store", default='None', dest='devfile')
parser.add_argument('trainfile', nargs='*')
results = parser.parse_args()

training_file_path = results.trainfile[0]
model_path = results.trainfile[1]
#model_path = sys.argv[2]
bag_of_words = dict()
w_vector = dict()
classes = list()
wscore = dict()
trainingset = 0
i = 0
maxerr = 0
trainfileforperc = "postraining"
with open(training_file_path, 'r') as tfile:
    with open(trainfileforperc, 'w') as wfile:
        for line in tfile:
            line = line.rstrip('\n')
            words = line.split(" ")
            i = 0
            for word in words:
                if i != 0 and i < len(words)-1:
                    try:
                        unit1 = words[i-1].split("/")
                        unit2 = words[i].split("/")
                        unit3 = words[i+1].split("/")
                        if len(unit1) is 2 and len(unit2) is 2 and len(unit3) is 2:
                            #wfile.write(unit[1]+" "+unit[0]+"\n")
                            wfile.write(unit2[1]+" "+"PREV_"+unit1[0]+" "+"CURR_"+unit2[0]+" "+"NEXT_"+unit3[0]+"\n")
                            #wfile.write(unit2[1]+" "+"CURR_"+unit2[0]+"\n")
                            #wfile.write(unit2[1]+" "+"NEXT_"+unit3[0]+"\n")
                    except IndexError:
                        continue
                elif i == 0 and i < len(words)-1:
                    try:
                        unit1 = words[i].split("/")
                        unit2 = words[i+1].split("/")
                        if len(unit1) == 2 and len(unit2) == 2:
                            wfile.write(unit1[1]+" "+"CURR_"+unit1[0]+" "+"NEXT_"+unit2[0]+"\n")
                            #wfile.write(unit1[1]+" "+"NEXT_"+unit2[0]+"\n")
                    except IndexError:
                        continue
                elif i == len(words)-1 and i > 0:
                    try:
                        unit1 = words[i].split("/")
                        unit2 = words[i-1].split("/")
                        if len(unit1) == 2 and len(unit2) == 2:
                            wfile.write(unit1[1]+" "+"CURR_"+unit1[0]+" "+"PREV_"+unit2[0]+"\n")
                            #wfile.write(unit1[1]"\n")
                    except IndexError:
                        continue
                i += 1



#os.system("python3 /home/shwetamouli/PycharmProjects/csci544-hw2/csci544-hw2/perceplearn.py postraining mod2")
os.system("python3 ../perceplearn.py postraining "+model_path)





