__author__ = 'shwetamouli'
import sys
import pickle

def keywithmaxval(d):
    v = list(d.values())
    k = list(d.keys())
    if len(v) != 0:
        return(k[v.index(max(v))])

model_path = sys.argv[1]
pickledata = pickle.load(open(model_path, "rb"))
bag_of_words = dict()
w_vector = pickledata[0]
classes = pickledata[1]
#wscore = dict()
posout = "posout1"
write = sys.stdout.write
#print(classes)
for line in sys.stdin:
    line = line.rstrip('\n')
    words = line.split(' ')
    i = 0
    # for word in words:
    #     wscore = 0
    #     wtag = None
    #     for classname in classes:
    #         #print(w_vector[classname])
    #         if "CURR_"+words[i] in w_vector[classname].keys():
    #             if w_vector[classname]["CURR_"+words[i]] > wscore:
    #                 #print(w_vector[classname]["CURR_"+word], wscore)
    #                 wscore = w_vector[classname]["CURR_"+words[i]]
    #                 wtag = classname
    #         # elif wtag == None and i != 0 and "PREV_"+words[i-1] in w_vector[classname].keys():
    #         #     if w_vector[classname]["PREV_"+words[i-1]] > wscore:
    #         #         wscore = w_vector[classname]["PREV_"+words[i-1]]
    #         #         wtag = classname
    #         # elif wtag == None and i != len(words)-1 and "NEXT_"+words[i+1] in w_vector[classname].keys():
    #         #     if w_vector[classname]["NEXT_"+words[i+1]] >wscore:
    #         #         wscore = w_vector[classname]["NEXT_"+words[i+1]]
    #         #         wtag = classname
    #     write = sys.stdout.write
    #     write(words[i]+"/"+str(wtag)+" ")
    #     sys.stdout.flush()
    #     i=i+1
    #

    for word in words:
        wscore = dict()
        wtag = None
        #print(len(words), words, i)
        if i != 0 and i < len(words)-1:
            prev = "PREV_"+words[i-1]
            curr = "CURR_"+words[i]
            nextw = "NEXT_"+words[i+1]
            for classname in classes:
                if prev in w_vector[classname] and curr in w_vector[classname] and nextw in w_vector[classname]:
                    wscore[classname] = w_vector[classname][prev] +w_vector[classname][curr] +w_vector[classname][nextw]
                    continue
                elif prev in w_vector[classname] and nextw in w_vector[classname]:
                    wscore[classname] = w_vector[classname][prev] +w_vector[classname][nextw]
                    continue
                elif curr in w_vector[classname] and nextw in w_vector[classname]:
                    wscore[classname] = w_vector[classname][curr] +w_vector[classname][nextw]
                    continue
                elif curr in w_vector[classname] and prev in w_vector[classname]:
                    wscore[classname] = w_vector[classname][curr] +w_vector[classname][prev]
                    continue
                elif nextw in w_vector[classname]:
                    wscore[classname] = w_vector[classname][nextw]
                    continue
                elif curr in w_vector[classname]:
                    wscore[classname] = w_vector[classname][curr]
                    continue
                elif prev in w_vector[classname]:
                    wscore[classname] = w_vector[classname][prev]
                    continue
        elif i == 0 and i < len(words)-1:
            curr = "CURR_"+words[i]
            nextw = "NEXT_"+words[i+1]
            for classname in classes:
                if nextw in w_vector[classname] and curr in w_vector[classname]:
                    wscore[classname] = w_vector[classname][nextw] +w_vector[classname][curr]
                    continue
                elif nextw in w_vector[classname]:
                    wscore[classname] = w_vector[classname][nextw]
                    continue
                elif curr in w_vector[classname]:
                    wscore[classname] = w_vector[classname][curr]
                    continue

        elif i == len(words)-1 and i > 0:
            prev = "PREV_"+words[i-1]
            curr = "CURR_"+words[i]
            for classname in classes:
                if prev in w_vector[classname] and curr in w_vector[classname]:
                    wscore[classname] = w_vector[classname][prev] +w_vector[classname][curr]
                    continue
                elif prev in w_vector[classname]:
                    wscore[classname] = w_vector[classname][prev]
                    continue
                elif curr in w_vector[classname]:
                    wscore[classname] = w_vector[classname][curr]
                    continue
        #print(wscore)
        if len(wscore.keys()) != 0:
            wtag = keywithmaxval(wscore)


        #sys.stdout.flush()
        i += 1
        if i == len(words)-1:
            write(word+"/"+str(wtag)+"\n")
            sys.stdout.flush()
        elif word is not "":
            write(word+"/"+str(wtag)+" ")
            sys.stdout.flush()


    #wscore = 0
    #write = sys.stdout.write



    # words = line.split(' ')
    # i = 0
    # prev_word_tag = None
    # for word in words:
    #     if i != 0:
    #         prev = words[i-1]
    #         curr = words[i]
    #         nextw = words[i+1]
    #         prev_wscore = 0
    #         if prev_word_tag is not None:
    #             try:
    #                 prev_wscore = w_vector[prev_word_tag]["PREV_"+str(prev)]
    #             except KeyError:
    #                 prev_wscore = 0
    #         next_wscore = 0
    #         next_word_tag = None
    #         for classname in classes:
    #             if "NEXT_"+nextw in w_vector[classname].keys():
    #                 if w_vector[classname]["NEXT_"+nextw] > next_wscore:
    #                     next_wscore = w_vector[classname]["NEXT_"+nextw]
    #                     next_word_tag = classname
    #         curr_wscore = 0
    #         curr_word_tag = None
    #         for classname in classes:
    #             for cword in w_vector[classname].keys():
    #                 if prev_word_tag is not None and next_word_tag is not None:
    #                     if "CURR_"+cword in w_vector[classname].keys():
    #                         curr_wscore =
    #
    #
    #
    #



#def classify()
#for number in range(0, 10):
#            word = line.rstrip('\n')

#            label = words.pop(0)
#             for entry in classes:
#                 try:
#                     wscore[entry] = 0
#                 except KeyError:
#                     wscore[entry] = 0
#             for classname in classes:
#                 #for word in words:
#                     #if label in w_vector.keys():
#                 if word in w_vector[classname].keys():
#                     try:
#                         wscore[classname] += w_vector[classname][word]
#                     except KeyError:
#                         wscore[classname] = w_vector[classname][word]
#             pred_class = keywithmaxval(wscore)
#             #print(wscore)
#             outfile.write(pred_class+" "+word+"\n")
#             #print(pred_class)
#
