```
# README #

I wrote an Averaged Perceptron as required for part 1 and it did pretty well on the SPAM and HAM data sets.  I have used for POS and NER tagging. I take advantage of the directory structure while calling the perceplearn from my pos and ner trainers and I hope this doesn't lead to issues.

1. I used my POS tagger on the Dev set and it is 94.73% accurate
   
2. The metrics for the NER tagger is as follows:
         ORG - Precision 0.554, Recall 0.909, F-Score 0.677
         PER - Precision 0.93, Recall 0.569, F-Score 0.706
         LOC - Precision 0.759, Recall 0.74, F-Score 0.749
         MISC - Precision 0.723, Recall 0.714, F-Score 0.718
         Overall Precision - 0.6406, Recall - 0.6441, F-score = 0.6423

3. Using Naive Bayes for the POS tagging gave me an accuracy of 86.04% - it seemed to do okay, but not as well as perceptron, perhaps because of the assumption of independence which is false in POS tagging. 
Using it for NER tagging yielded similar results as the perceptron with the scores being almost as poor. I got an overall precision of 0.583, recall 0.6015, F-score 0.5921
I noticed that though it got come of the B-tags right it did not make the next I connection. This might be because of the way Naive Bayes works - again because of the independence assumption.