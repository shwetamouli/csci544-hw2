__author__ = 'shwetamouli'
import pickle
import sys
from fileshuffler import fileshuffle
import argparse

parser = argparse.ArgumentParser(description='Adding heldout dev file')
parser.add_argument('-hd', action="store", default='None', dest='devfile')
parser.add_argument('trainfile', nargs='*')
results = parser.parse_args()

def keywithmaxval(d):
    v = list(d.values())
    k = list(d.keys())
    return(k[v.index(max(v))])

#training_file_path = sys.argv[1]
#training_file = open(training_file_path, 'r')
training_file_path = results.trainfile[0]
model_path = results.trainfile[1]
#model_path = sys.argv[2]
bag_of_words = dict()
w_vector = dict()
classes = list()
wscore = dict()
trainingset = 0
i = 0
maxerr = 0

for number in range(0, 50):
    i += 1

    fileshuffle(training_file_path)
    with open(training_file_path, 'r', encoding="iso-8859-15") as tfile:
        for line in tfile:
            line = line.rstrip('\n')
            words = line.split(' ')
            label1 = words[0]
            #print(words[0], label1)
            words.pop(0)
            trainingset += len(words)
            #print(classes)
            if label1 not in classes:
                classes.append(label1)
            if label1 not in w_vector.keys():
                w_vector[label1] = dict()
            for label in classes:
                wscore[label] = 0
            for label in classes:
                for word in words:
                    if word in w_vector[label].keys():
                        try:
                            wscore[label] += w_vector[label][word]
                        except KeyError:
                            wscore[label] = w_vector[label][word]
            #print(w_vector)
            #print(wscore)
            pred_class = keywithmaxval(wscore)
            #print(label1, pred_class)
            if label1 == pred_class:
                continue
            else:
                for word in words:
                    try:
                        w_vector[label1][word] += 1
                    except KeyError:
                        w_vector[label1][word] = 1
                    try:
                        w_vector[pred_class][word] -= 1
                    except KeyError:
                        w_vector[pred_class][word] = -1
    classpred = dict()
    classact = dict()
    for entry in classes:
        classpred[entry] = 0
        classact[entry] = 0
    if results.devfile != 'None':

        with open(results.devfile, 'r') as devfile:
            for line in devfile:
                words = line.split(' ')
                label = words.pop(0)
                for entry in classes:
                    wscore[entry] = 0
                    # try:
                    #     wscore[entry] = 0
                    # except KeyError:
                    #     wscore[entry] = 0
                for classname in classes:
                    for word in words:
                        #if label in w_vector.keys():
                        if word in w_vector[classname].keys():
                            try:
                                wscore[classname] += w_vector[classname][word]
                            except KeyError:
                                wscore[classname] = w_vector[classname][word]

                pred_class = keywithmaxval(wscore)
                classact[label] += 1
                if label == pred_class:
                    classpred[pred_class] += 1
                #print(wscore)
                #print(pred_class)
            precision = list()
            for entry in classes:
                precision.append((float)(classpred[entry])/(float)(classact[entry]))
            errorrate = (float)(sum(precision)) / (float)(len(precision))

        if errorrate > maxerr:
            pickledata = [w_vector, classes]
            pickle.dump(pickledata, open(model_path, "wb"))

        print("Iteration: " + str(i) + " Error rate: " + str(errorrate))
    else:
        w_avgvector =dict()
        for wavgk in w_vector:
            for wvword in w_vector[wavgk]:
                try:
                    w_avgvector[wavgk][wvword] = (float)(w_vector[wavgk][wvword])/(float)(trainingset)
                except KeyError:
                    w_avgvector[wavgk] = dict()
                    w_avgvector[wavgk][wvword] = (float)(w_vector[wavgk][wvword])/(float)(trainingset)

        pickledata = [w_avgvector, classes]
        pickle.dump(pickledata, open(model_path, "wb"))
