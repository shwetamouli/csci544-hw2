__author__ = 'shwetamouli'
import sys
import pickle
import codecs
import io

def keywithmaxval(d):
    v = list(d.values())
    k = list(d.keys())
    if len(v) != 0:
        return(k[v.index(max(v))])

model_path = sys.argv[1]
pickledata = pickle.load(open(model_path, "rb"))
bag_of_words = dict()
w_vector = pickledata[0]
classes = pickledata[1]
#wscore = dict()
posout = "nerout1"
write = sys.stdout.write
#reader = codecs.getreader(encoding="iso-8859-15")
#sys.stdin = reader(sys.stdin)
#sys.stdout = reader(sys.stdout)
input_stream = io.TextIOWrapper(sys.stdin.buffer, encoding="iso-8859-15")
#print(classes)
for line in input_stream:
    line = line.rstrip('\n')
    words = line.split(' ')
    i = 0
    for word in words:
        wscore = dict()
        wtag = None
        #print(len(words), words, i)
        if i != 0 and i < len(words)-1:
            prev = "PREV_"+words[i-1]
            curr = "CURR_"+words[i]
            nextw = "NEXT_"+words[i+1]
            for classname in classes:
                if prev in w_vector[classname] and curr in w_vector[classname] and nextw in w_vector[classname]:
                    wscore[classname] = w_vector[classname][prev] +w_vector[classname][curr] +w_vector[classname][nextw]
                    continue
                elif prev in w_vector[classname] and nextw in w_vector[classname]:
                    wscore[classname] = w_vector[classname][prev] +w_vector[classname][nextw]
                    continue
                elif curr in w_vector[classname] and nextw in w_vector[classname]:
                    wscore[classname] = w_vector[classname][curr] +w_vector[classname][nextw]
                    continue
                elif curr in w_vector[classname] and prev in w_vector[classname]:
                    wscore[classname] = w_vector[classname][curr] +w_vector[classname][prev]
                    continue
                elif nextw in w_vector[classname]:
                    wscore[classname] = w_vector[classname][nextw]
                    continue
                elif curr in w_vector[classname]:
                    wscore[classname] = w_vector[classname][curr]
                    continue
                elif prev in w_vector[classname]:
                    wscore[classname] = w_vector[classname][prev]
                    continue
        elif i == 0 and i < len(words)-1:
            curr = "CURR_"+words[i]
            nextw = "NEXT_"+words[i+1]
            for classname in classes:
                if nextw in w_vector[classname] and curr in w_vector[classname]:
                    wscore[classname] = w_vector[classname][nextw] +w_vector[classname][curr]
                    continue
                elif nextw in w_vector[classname]:
                    wscore[classname] = w_vector[classname][nextw]
                    continue
                elif curr in w_vector[classname]:
                    wscore[classname] = w_vector[classname][curr]
                    continue

        elif i == len(words)-1 and i > 0:
            prev = "PREV_"+words[i-1]
            curr = "CURR_"+words[i]
            for classname in classes:
                if prev in w_vector[classname] and curr in w_vector[classname]:
                    wscore[classname] = w_vector[classname][prev] +w_vector[classname][curr]
                    continue
                elif prev in w_vector[classname]:
                    wscore[classname] = w_vector[classname][prev]
                    continue
                elif curr in w_vector[classname]:
                    wscore[classname] = w_vector[classname][curr]
                    continue
        #print(wscore)
        if len(wscore.keys()) != 0:
            wtag = keywithmaxval(wscore)
        if wtag == None:
            for classname in classes:
                if word is not '':
                    if word.rsplit('/', 1)[1] in w_vector[classname]:
                        wscore[classname] = w_vector[classname][word.rsplit('/', 1)[1]]

            if len(wscore.keys()) != 0:
                wtag = keywithmaxval(wscore)


        #sys.stdout.flush()
        i += 1
        if i == len(words)-1:
            write(word+"/"+str(wtag)+"\n")
            sys.stdout.flush()
        elif word is not "":
            write(word+"/"+str(wtag)+" ")
            sys.stdout.flush()

