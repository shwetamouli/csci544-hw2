__author__ = 'shwetamouli'
import pickle
import math
import sys

def keywithmaxval(d):
    v = list(d.values())
    k = list(d.keys())
    return(k[v.index(max(v))])

#training_file_path = sys.argv[1]
#training_file = open(training_file_path, 'r')
model_path = sys.argv[1]
pickledata = pickle.load(open(model_path, "rb"))
bag_of_words = dict()
w_vector = pickledata[0]
classes = pickledata[1]
wscore = dict()

#with open(training_file_path, 'r') as tfile:
    #tfile1 = list(tfile).shuffle()
#    for line in tfile:
for line in sys.stdin:
    words = line.split(' ')
    # if words[0] not in w_vector.keys():
    #     w_vector[words[0]] = dict()
    #     classes.append(words[0])
    label = words.pop(0)
    for entry in classes:
        try:
            wscore[entry] = 0
        except KeyError:
            wscore[entry] = 0
    for classname in classes:
        for word in words:
            #if label in w_vector.keys():
            if word in w_vector[classname].keys():
                try:
                    wscore[classname] += w_vector[classname][word]
                except KeyError:
                    wscore[classname] = w_vector[classname][word]
    pred_class = keywithmaxval(wscore)
    #print(wscore)
    write = sys.stdout.write
    write(pred_class+"\n")
    sys.stdout.flush()

#pickle.dump(w_vector, open(nb_model_path, "wb"))

