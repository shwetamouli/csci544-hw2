__author__ = 'shwetamouli'
import random
def fileshuffle(filepath):
    with open(filepath,'r', encoding="iso-8859-15") as source:
        data = [(random.random(), line) for line in source]
    data.sort()
    with open(filepath, 'w', encoding="iso-8859-15") as target:
        for _, line in data:
            target.write(line)
